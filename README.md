# worldmedia

## To Do
- Resolve font decode error
- Refactor CSS selectors by taking advantage of sass nesting
- Media query to ensure card images are positioned correctly on larger screens

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
